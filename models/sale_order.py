# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields
from datetime import datetime

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    vracoop_point_retrait_id = fields.Many2one(
        comodel_name='vracoop.point.retrait', 
        string="Point retrait")
    day_retrait = fields.Date("Jour du retrait")
    hour_retrait = fields.Float("Heure du retrait")
    carrier_point_retrait = fields.Boolean(
        string='Point retrait',
        related='carrier_id.point_retrait')

    def _check_retrait(self, force_retrait_id=None):
        self.ensure_one()
        PointRetrait = self.env['vracoop.point.retrait']

        retrait = force_retrait_id and PointRetrait.browse(force_retrait_id)
        self.write({'vracoop_point_retrait_id': retrait.id})
        
        return bool(retrait)

    def _update_day_hour(self, hour_retrait=None):
        self.ensure_one()
        
        today = datetime.today()
        values = hour_retrait.split("-")
        day_retrait = datetime(today.year, int(values[1]), int(values[0]))

        hour = values[2].split(":")
        hour_retrait = float('%s.%s' % (hour[0], hour[1]))

        PointRetrait = self.env['vracoop.point.retrait']
        retrait = PointRetrait.browse(values[3])
        self.write({
            'hour_retrait': hour_retrait, 
            'day_retrait': day_retrait,
            'vracoop_point_retrait_id': retrait.id})
        
        return True
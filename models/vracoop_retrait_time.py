# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class VracoopRetraitTime(models.Model):
    _name = "vracoop.retrait.time"
    _description = "Informations horaires par retrait"

    vracoop_time_id = fields.Many2one('vracoop.time', 'Conf time')
    name = fields.Selection([
        (1, 'Lundi'),
        (2, 'Mardi'),
        (3, 'Mercredi'),
        (4, 'Jeudi'),
        (5, 'Vendredi'),
        (6, 'Samedi'),
        (0, 'Dimanche')], 
        string='Jour de la semaine')
    # name = fields.Selection([
    #     ('lundi', 'Lundi'),
    #     ('mardi', 'Mardi'),
    #     ('mercredi', 'Mercredi'),
    #     ('jeudi', 'Jeudi'),
    #     ('vendredi', 'Vendredi'),
    #     ('samedi', 'Samedi'),
    #     ('dimanche', 'Dimanche')], 
    #     string='Jour de la semaine')
    first_morning_heure = fields.Float(string='Heure du 1er retrait (matin)')
    last_morning_heure = fields.Float(
        string='Heure du dernier retrait (matin)')
    first_noon_heure = fields.Float(string='Heure du 1er retrait (après-midi)')
    last_noon_heure = fields.Float(
        string='Heure du dernier retrait (après-midi)')
    preparation_time = fields.Float(string='Temps de préparation')
    availability_time = fields.Float(string='Temps de mise à disposition')
    active_day = fields.Boolean("Actif")
    vracoop_point_retrait_id = fields.Many2one(
        comodel_name='vracoop.point.retrait', 
        string="Point retrait")


class VracoopTime(models.Model):
    _name = "vracoop.time"
    _description = "Configuration horaires de base"

    name = fields.Selection([
        (1, 'Lundi'),
        (2, 'Mardi'),
        (3, 'Mercredi'),
        (4, 'Jeudi'),
        (5, 'Vendredi'),
        (6, 'Samedi'),
        (0, 'Dimanche')], 
        string='Jour de la semaine')
    first_morning_heure = fields.Float(string='Heure du 1er retrait (matin)')
    last_morning_heure = fields.Float(
        string='Heure du dernier retrait (matin)')
    first_noon_heure = fields.Float(string='Heure du 1er retrait (après-midi)')
    last_noon_heure = fields.Float(
        string='Heure du dernier retrait (après-midi)')
    preparation_time = fields.Float(string='Temps de préparation')
    availability_time = fields.Float(string='Temps de mise à disposition')
# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models, api


class VracoopDeliveryCarrier(models.Model):
    _inherit = 'delivery.carrier'

    type_carrier = fields.Selection([
        ('retrait', 'Point retrait')], string="Type")
    point_retrait = fields.Boolean(string='Point retrait')

    @api.onchange('type_carrier')
    def onchange_type_carrier(self):
        for delivery in self:
            if delivery.type_carrier == 'retrait':
                delivery.point_retrait = True
            else:
                delivery.point_retrait = False
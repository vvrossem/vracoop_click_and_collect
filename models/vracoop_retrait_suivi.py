# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class VracoopRetraitSuivi(models.Model):
    _name = "vracoop.retrait.suivi"
    _description = "Suivi des commandes en retrait"

    order_id = fields.Many2one('sale.order', "Numéro bon de commande")
    partner_id = fields.Many2one('res.partner', "Client")
    vracoop_point_retrait_id = fields.Many2one(
        comodel_name='vracoop.point.retrait', 
        string="Point retrait")
    day_retrait = fields.Selection([
        ('lundi', 'Lundi'),
        ('mardi', 'Mardi'),
        ('mercredi', 'Mercredi'),
        ('jeudi', 'Jeudi'),
        ('vendredi', 'Vendredi'),
        ('samedi', 'Samedi'),
        ('dimanche', 'Dimanche')], 
        string='Jour du retrait')
    hour_retrait = fields.Float(string='Heure du retrait')
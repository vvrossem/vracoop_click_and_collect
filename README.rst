.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


=================================
VRACOOP - Processus Click&Collect
=================================

Modifie le module Website Sale:
    - ajout d'un modèle Retrait pour gérer les points de retrait 
    - ajout d'un bouton dans le menu Site / des vues associées à la gestion des points de Relais

Credits
=======

Contributors
------------

* Juliana Poudou <juliana@le-filament.com>

Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament

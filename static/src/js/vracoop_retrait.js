odoo.define('vracoop_click_and_collect.vracoop_retrait', function (require) {
    'use strict';

    require('web.dom_ready');
    var ajax = require('web.ajax');
    var core = require('web.core');
    var _t = core._t;
    var concurrency = require('web.concurrency');
    var dp = new concurrency.DropPrevious();

    $('#carousel_1').on('slide.bs.carousel', function (e) {
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 5;
        var totalItems = $('.carousel-item').length;
     
        if (idx >= totalItems-(itemsPerSlide-1)) {
            var it = itemsPerSlide - (totalItems - idx);
            for (var i=0; i<it; i++) {
                // append slides to end
                if (e.direction=="left") {
                    $('.carousel-item').eq(i).appendTo('.carousel-inner');
                }
                else {
                    $('.carousel-item').eq(0).appendTo('.carousel-inner');
                }
            }
        }
    });

    var $pay_button = $('#o_payment_form_pay');

    var _onRetraitUpdateAnswer = function(result) {
        var $retrait_badge = $('#point_retrait input[name="point_retrait_type"][value=' + result.vracoop_point_retrait_id + ']');
    };

    var _onRetraitClick = function(ev) {
        var vracoop_point_retrait_id = $(ev.currentTarget).val();
        var values = {'vracoop_point_retrait_id': vracoop_point_retrait_id};
        dp.add(ajax.jsonRpc('/shop/update_retrait', 'call', values))
          .then(_onRetraitUpdateAnswer);
    };

    var $retraits = $("#point_retrait input[name='point_retrait_type']");
    $retraits.click(_onRetraitClick);
    
    if ($retraits.length > 0) {
        $retraits.filter(':checked').click();
    }
    
    var _onDayUpdateAnswer = function(result) {
        
    };

    var _onDayClick = function(ev) {
        var day_retrait = $(ev.currentTarget).val();
        var values = {'day_retrait': day_retrait};
        dp.add(ajax.jsonRpc('/shop/update_retrait', 'call', values))
          .then(_onDayUpdateAnswer);
    };

    var $days = $("#point_retrait input[name='day_select']");
    $days.click(_onDayClick);
    
    if ($days.length > 0) {
        $days.filter(':checked').click();
    }

    var _onHourUpdateAnswer = function(result) {
        if (result.status === true) {
            $pay_button.data('disabled_reasons').retrait_selection = false;
            $pay_button.prop('disabled', _.contains($pay_button.data('disabled_reasons'), true));
        }
        else {
            console.error(result.error_message);
        }
    };

    var _onHourClick = function(ev) {
        var hour_retrait = $(ev.currentTarget).val();
        var values = {'hour_retrait': hour_retrait};
        dp.add(ajax.jsonRpc('/shop/update_retrait', 'call', values))
          .then(_onHourUpdateAnswer);
    };

    var $hours = $("#point_retrait input[name='hour_select']");
    $hours.click(_onHourClick);
    
    if ($hours.length > 0) {
        $hours.filter(':checked').click();
    }

    var _onCarrierCheckType = function(result) {
        var $carrier_badge = $('#delivery_carrier input[name="delivery_type"][value=' + result.carrier_id + '] ~ .badge.d-none');
        var $point_retrait = $('#point_retrait');
        if (result.point_retrait === true) {
            $point_retrait.removeClass('d-none');
            $pay_button.data('disabled_reasons', $pay_button.data('disabled_reasons') || {});
            $pay_button.prop('disabled', true);
            $pay_button.data('disabled_reasons').retrait_selection = true;
        }
        else {
            $pay_button.data('disabled_reasons').retrait_selection = false;
            $pay_button.prop('disabled', _.contains($pay_button.data('disabled_reasons'), true));
            $point_retrait.addClass('d-none');
        }
    };

    var _onCarrierClick2 = function(ev) {
        var carrier_id = $(ev.currentTarget).val();
        var values = {'carrier_id': carrier_id};
        dp.add(ajax.jsonRpc('/shop/check_type_carrier', 'call', values))
          .then(_onCarrierCheckType);
    };

    var $carriers = $("#delivery_carrier input[name='delivery_type']");
    $carriers.click(_onCarrierClick2);

    if ($carriers.length > 0) {
        $carriers.filter(':checked').click();
    }
    
});
